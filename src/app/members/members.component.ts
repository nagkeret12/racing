import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { NgxConfirmBoxService } from 'ngx-confirm-box';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];
   idToDelete:number;
  constructor(public appService: AppService, private router: Router,private confirmBox: NgxConfirmBoxService) {}


  bgColor           ='rgba(0,0,0,0.5)'; // overlay background color
  confirmHeading    = '';
  confirmContent    = "Are you sure want to delete tsddshis?";
  confirmCanceltext = "Cancel";
  confirmOkaytext   = "Okay";
 
    yourMethod(){
        this.confirmBox.show();
    }
 
    confirmChange(showConfirm:boolean){
        if(showConfirm){
          this.appService.deleteMember(this.idToDelete).subscribe(members =>{
            console.log('members'+members);
            this.ngOnInit();
          });
        }
      }
 


  ngOnInit() {
    this.appService.getMembers().subscribe(members =>{
     (this.members = members)
    });
     if(this.appService.username == null)
    {
      this.router.navigate(['']);
    }
  }

  goToAddMemberForm() {
    localStorage.removeItem("id");
    this.router.navigate(['membersDetails']);
  }

  editMemberByID(id:number) {
    localStorage.removeItem("id");
    localStorage.setItem("id", id.toString());
    this.router.navigate(['editMembersDetails']);
  }

  deleteMemberByID(id:number) {
    this.confirmBox.show();
    this.idToDelete=id;
    // if(confirm("Are you sure to delete Member Id: "+id)) {
    //   this.appService.deleteMember(id).subscribe(members =>{
    //     console.log('members'+members);
    //     this.ngOnInit();
    //   });
    //}
  }
}
