import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.appService.username = null;
    localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }

  navigateToMembers()
  {
    if(isNullOrUndefined(this.appService.username))
        this.router.navigate(['login'])
        else
   this.router.navigate(['members']);
  }
}
