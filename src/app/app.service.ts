import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

export interface Member {
  id:number;
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

const BASE_URL = 'http://localhost:3004/api';
const USERNAME = 'admin';
const PASSWORD = 'Admin123';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  username:string;
  constructor(private http: HttpClient) {
  }

//User Validation 
  validateUser(userName:string,password:string){
    return  userName.toLowerCase() === USERNAME && password === PASSWORD
  }

  //To set the logged in userDetails in Globle service
  setUsername(name: string): void {
    this.username = name;
  }

  // APIs for Member CRUP operations
  getMembers() {
    return this.http
      .get(`${BASE_URL}/members`)
      .pipe(catchError(this.handleError));
  }

  getMembersByID(id:number) {
    return this.http
      .get(`${BASE_URL}/members/${id}`)
      .pipe(catchError(this.handleError));
  } 

  addMember(member :Member) {
    return this.http
      .post(`${BASE_URL}/members`,member)
      .pipe(catchError(this.handleError));
  }

  updateMember(member :Member) {
    return this.http
      .put(`${BASE_URL}/members/${member.id}`,member)
      .pipe(catchError(this.handleError));
  }

  deleteMember(id :number) {
    return this.http
      .delete(`${BASE_URL}/members/${id}`)
      .pipe(catchError(this.handleError));
  }


  //Team APIs
  getTeams() {
    return this.http
    .get(`${BASE_URL}/teams`)
    .pipe(catchError(this.handleError));
  }


  //To handle Error
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } 
    else {      
      
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
      throw new Error(error.error.details[0].message);
    } 
    return [];
  }
}
