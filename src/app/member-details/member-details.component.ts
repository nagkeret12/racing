import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { AppService,Member } from '../app.service';
import { Router } from '@angular/router';

// This interface may be useful in the times ahead...


@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  newMember = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  errmsg:string;
  constructor(private fb: FormBuilder, private appService: AppService, private router: Router) {}

  addForm: FormGroup;
  ngOnInit() {
    this.appService.getTeams().subscribe(teams => {
      this.teams = teams;
      console.log(teams.count);
      console.log(JSON.stringify(this.teams));
    });

     this.addForm = this.fb.group({
     lastName: new FormControl('', Validators.required),
     firstName: new FormControl('', Validators.required),
     jobTitle: new FormControl('', Validators.required),
     team: new FormControl('', Validators.required),
     status: new FormControl('', Validators.required)});   
  }

  onSubmit() {
    this.appService.addMember(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['members']);
      },err=>{
        // if(err && err.error && err.error.details[0])
        // this.errmsg=err.error.details[0].message;
        // else
         this.errmsg=err;
        console.log('error occured'+err);
      });
  }
}
