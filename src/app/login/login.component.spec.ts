import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { LoginComponent } from './login.component';

import { HttpClient } from '@angular/common/http';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { AppService } from '../app.service';


class  MockAppService extends AppService {
authenticated=false;

validateUser(userName,password)
{
  return this.authenticated;
}

}

formBuilder : FormBuilder;
router: Router;
appService: AppService;
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service:MockAppService;
  let appService: AppService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, RouterModule, HttpClientModule],
      providers: [
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        },
        HttpClient
      ]
    }).compileComponents();
  }));

  
  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      providers: [
        {provide: AppService, useClass: MockAppService}
      ]
    });
    component = TestBed.createComponent(LoginComponent).componentInstance;
    appService = TestBed.get(AppService);
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  fit('should success for invalid details', () => {
    service.authenticated=true;
    expect(component.login()).toBeTruthy();
  });
  it('should falied for invalid details', () => {
    service.authenticated=false;
    expect(component.login()).toBeFalsy();
  });
});
