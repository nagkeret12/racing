import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  showErrorMessage = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private appService: AppService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    if(!this.appService.validateUser(this.loginForm.value.username,this.loginForm.value.password))
        this.showErrorMessage = true;    
    else {    
        this.appService.setUsername(this.loginForm.value.username);
        this.router.navigate(['/members']);
    }}
}

