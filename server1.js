const Joi = require('joi');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');
const app = express();

const JsonServerURL="http://localhost:3000/";
const API = {
  MEMBERS: 'members',
  TEAMS: 'teams'
}

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

//APIs for members CRUD operations

//GET ALL
app.get('/api/members', (req, res) => {
  const URL=`${JsonServerURL + API.MEMBERS}`;
  request(URL, (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

//GET 
app.get('/api/members/:id', (req, res) => {
  const URL=`${JsonServerURL + API.MEMBERS}/${req.params.id}`;
  request(URL, (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});


//DELETE
app.delete('/api/members/:id', (req, res) => {
  request({
    url: `${JsonServerURL + API.MEMBERS}/${req.params.id}`,
    method: "DELETE",
    json: true    
  }, function (error, response, body){
    res.send(body)
  });
});

//POST
app.post('/api/members', (req, res) => {
 const { error } = ValidateMember(req.body);
 console.log('server'+ error);
 if(error) return res.status(400).send(error);
 request({
    url: `${JsonServerURL + API.MEMBERS}`,
    method: "POST",
    json: true,
    body: req.body
  },function (error, response, body){
    res.send(body)
  });
});


//PUT
app.put('/api/members/:id', (req, res) => {
  const { error } = ValidateMember(req.body);
  console.log(" val "+error);
 if(error) return res.status(400).send(error);
    request({
      url: `${ JsonServerURL + API.MEMBERS }/${req.params.id}`,
      method: "PUT",
      json: true , 
      body:req.body
  }, function (error, response, body){
      res.send(body)
  });
});

// API for Get List of teams
app.get('/api/teams', (req, res) => {
  
  request(`${ JsonServerURL + API.TEAMS }`, (err, response, body) => {
    if (response.statusCode <= 500) {
      console.log(JSON.stringify(body));
      res.send(body);
    }
  });
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});


function ValidateMember(member)
{
    const schema = Joi.object().keys(
    {
      firstName: Joi.string().required().regex(/^[a-zA-Z]{3,30}$/),
      lastName: Joi.string().required().regex(/^[a-zA-Z]{3,30}$/),
      jobTitle: Joi.string().required().alphanum().min(3).max(50),
      team: Joi.string().required().min(3).max(50),
      status: Joi.string().required(),
      id:Joi.number().optional()
    });
    console.log(Joi.validate(member,schema));
   return Joi.validate(member,schema);
}

app.listen('3004', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});