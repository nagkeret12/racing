import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { AppService,Member } from '../app.service';
import { Router } from '@angular/router';

// This interface may be useful in the times ahead...


@Component({
  selector: 'app-eidt-member-details',
  templateUrl: './edit-member-details.component.html',
  styleUrls: ['./edit-member-details.component.css']
})
export class EditMemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  newMember = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  errmsg:string;
  constructor(private formBuilder: FormBuilder, private appService: AppService, private router: Router) {}
  editForm: FormGroup;
  ngOnInit() {
    this.appService.getTeams().subscribe(teams => {
      this.teams = teams;
    });


    let userId = window.localStorage.getItem("id");
    if(!userId) {
      alert("Invalid action.");
    }
    this.editForm = this.formBuilder.group({
          id: [''],
          lastName: new FormControl('', Validators.required),
          firstName: new FormControl('', Validators.required),
          jobTitle: new FormControl('', Validators.required),
          team: new FormControl('', Validators.required),
          status: new FormControl('', Validators.required)});

    this.appService.getMembersByID(+userId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
    }   

  ngOnChanges() {}

  // TODO: Add member to members
  onSubmit() {
    this.appService.updateMember(this.editForm.value)
      .subscribe( data => {
        this.router.navigate(['members']);
      },err=>{
        // if(err && err.error && err.error.details[0])
        // this.errmsg=err.error.details[0].message;
        // else
         this.errmsg=err;
        console.log('error occured'+err);
      });
  }
}
